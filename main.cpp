#include <iostream>

/////////////////////////////////////////////////////////////////////////////////////////
//// VikBo - pattern Builder
/////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////
//// Different Classes
/////////////////////////////////////////////////////////////////////////////////////////

// Class that describe radial bomb damage
class RadialBombDamage
{
private:
    int RadiusDamageIntensity{0};
    int MaxDamageIntensity{0};
    int MinDamageIntensity{0};
    std::string DamageParticleEffect;
public:
    RadialBombDamage()= default;
    ~RadialBombDamage()= default;

    friend std::ostream& operator<<(std::ostream& out, const RadialBombDamage& MyObjRadialBombDamage)
    {
        out << "\n# Is a RadialBombDamage. Object Characteristics:\n\n";
        out << "RadiusDamageIntensity=" << MyObjRadialBombDamage.RadiusDamageIntensity << std::endl;
        out << "MaxDamageIntensity=" << MyObjRadialBombDamage.MaxDamageIntensity << std::endl;
        out << "MinDamageIntensity=" << MyObjRadialBombDamage.MinDamageIntensity << std::endl;
        out << "DamageParticleEffect=" << MyObjRadialBombDamage.DamageParticleEffect << std::endl;
        return out;
    }

friend class BuilderRadialBombDamage;
};


// Class that describe radial sound propagation
class RadialSound
{
private:
    int RadiusSoundIntensity{0};
    int MaxSoundIntensity{0};
    int MinSoundIntensity{0};
public:
    RadialSound() = default;
    ~RadialSound() = default;

    friend std::ostream& operator<<(std::ostream& out, const RadialSound& MyObjRadialSound )
    {
        out << "\n# Is a RadialSound. Object Characteristics:\n\n";
        out << "RadiusSoundIntensity=" << MyObjRadialSound.RadiusSoundIntensity << std::endl;
        out << "MaxSoundIntensity=" << MyObjRadialSound.MaxSoundIntensity << std::endl;
        out << "MinSoundIntensity=" << MyObjRadialSound.MinSoundIntensity << std::endl;
        return out;
    }


friend class BuilderRadialSound;
};


/////////////////////////////////////////////////////////////////////////////////////////
//// Common Interface for classes BuilderRadialBombDamage and BuilderRadialSound
/////////////////////////////////////////////////////////////////////////////////////////
class IRadialAction
{
public:
    virtual void SetRadiusOfAction() = 0;
    virtual void SetMaxIntensity() = 0;
    virtual void SetMinIntensity() = 0;
    virtual void SetParticleEffect() = 0;
};

/////////////////////////////////////////////////////////////////////////////////////////
//// Builder for class RadialBombDamage
/////////////////////////////////////////////////////////////////////////////////////////
class BuilderRadialBombDamage : public IRadialAction
{
private:
    std::unique_ptr<RadialBombDamage> MyBombDamage;

public:
    BuilderRadialBombDamage()
    {
        MyBombDamage = std::make_unique<RadialBombDamage>(RadialBombDamage());
    }

    void SetRadiusOfAction() override {
        MyBombDamage->RadiusDamageIntensity = 100;
    }

    void SetMaxIntensity() override {
        MyBombDamage->MaxDamageIntensity = 20;
    }

    void SetMinIntensity() override {
        MyBombDamage->MinDamageIntensity = 5;
    }

    void SetParticleEffect() override {
        MyBombDamage->DamageParticleEffect = "splash!";
    }

    std::unique_ptr<RadialBombDamage> GetRadialAction() {
        return std::move(MyBombDamage);
    }

};


/////////////////////////////////////////////////////////////////////////////////////////
//// Builder for class RadialSound
/////////////////////////////////////////////////////////////////////////////////////////
class BuilderRadialSound : public IRadialAction
{
private:
    std::unique_ptr<RadialSound> MyRadialSound;

public:
    BuilderRadialSound(){
        MyRadialSound = std::make_unique<RadialSound>(RadialSound());
    }

    void SetRadiusOfAction() override {
        MyRadialSound->RadiusSoundIntensity = 300;
    }

    void SetMaxIntensity() override {
        MyRadialSound->MaxSoundIntensity = 79;
    }

    void SetMinIntensity() override {
        MyRadialSound->MinSoundIntensity = 10;
    }

    void SetParticleEffect() override {};


    std::unique_ptr<RadialSound> GetRadialAction() {
        return std::move(MyRadialSound);
    }

};


/////////////////////////////////////////////////////////////////////////////////////////
//// Director that manage our Builders
/////////////////////////////////////////////////////////////////////////////////////////
class DirectorRadial
{
public:
    static void constructRadialAction(IRadialAction* RadialAction)
    {
        RadialAction->SetRadiusOfAction();
        RadialAction->SetMaxIntensity();
        RadialAction->SetMinIntensity();
        RadialAction->SetParticleEffect();
    }
};


int main() {

    // create unique_ptr for Different Classes
    std::unique_ptr<RadialBombDamage> MyRadialBombDamage;
    std::unique_ptr<RadialSound> MyRadialSound;

    // create unique_ptr for Builders of this different classes
    std::unique_ptr<BuilderRadialBombDamage> MyBuilderRadialBombDamage;
    std::unique_ptr<BuilderRadialSound> MyBuilderRadialSound;


    MyBuilderRadialBombDamage = std::make_unique<BuilderRadialBombDamage>(BuilderRadialBombDamage());
    MyBuilderRadialSound = std::make_unique<BuilderRadialSound>(BuilderRadialSound());

    DirectorRadial::constructRadialAction(static_cast<IRadialAction*>(MyBuilderRadialBombDamage.get()));
    DirectorRadial::constructRadialAction(static_cast<IRadialAction*>(MyBuilderRadialSound.get()));

    MyRadialBombDamage = MyBuilderRadialBombDamage->GetRadialAction();
    MyRadialSound = MyBuilderRadialSound->GetRadialAction();

    if (MyRadialBombDamage)
    {
        std::cout << *MyRadialBombDamage;
    }

    if (MyRadialSound)
    {
        std::cout << *MyRadialSound;
    }

    return 0;
}
